# rbcat
`rbcat` is a lolcat clone in go that also adds a document reading (built-in cat command) functionality.

## How to use it
### Via the built-in cat function
```bash
rbcat yourDoc.txt
```

### Via piped input command
```bash
cat yourDoc.txt | rbcat
```

### Via piped output command
```bash
rbcat yourDoc.txt | bat
```

## Installation
### Arch Linux / Arch-based distros
1. AUR:
`yay -S rbcat` or `yay -S rbcat-bin` (same with paru)

2. building it yourself:
**You have to install go for that**
```bash
git clone https://git.minecodes.de/thies/rbcat
cd rbcat
go install
go build -o rbcat main.go
sudo mv rbcat /usr/bin/
```

### Other distros
```bash
git clone https://git.minecodes.de/thies/rbcat
cd rbcat
go install
go build -o rbcat main.go
sudo mv rbcat /usr/bin/
```
